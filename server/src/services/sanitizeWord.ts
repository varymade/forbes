export default function sanitizeWord(word: string): string {
    return word.replace(/[\W_]/, "").replace("\n", "");
}
