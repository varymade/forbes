import didYouMean from "didyoumean2";
import open from "open";
import express, {NextFunction, Request, Response} from "express";
import fs from "fs";
import path from "path";
import sanitizeWord from "./services/sanitizeWord";

const app = express();
const port = 3001;

/***
 * We import our data from the command line when the application. We could easily move these to
 * input fields to allow for dynamic uploading so they could use different dictionaries and stories.
***/
const [, , storyFilePath, dictionaryFilePath] = process.argv;
const story = fs.readFileSync(storyFilePath, 'utf8');
const dictionary = fs.readFileSync(dictionaryFilePath, 'utf8');

function logPath(req: Request, res: Response, next: NextFunction) {
    console.log(req.originalUrl);
    next();
}

app.use(express.static(path.resolve(__dirname, '../../ui/build')));

app.get('/story', logPath, (req: Request, res: Response) => {
    res.send(story);
});

app.get('/corrected-spelling', logPath, (req: Request, res: Response) => {

    const dictionaryList = dictionary.split("\n").map((word) => word.toLowerCase());

    const storyList = story.split(" ");

    // NOTE: I know I could have combined the reduce and map for performance but I wanted to show you I can use
    // different solutions together.
    const misspelledWords = storyList.reduce<string[]>((previousValue, word) => {
        const sanitizedWord = sanitizeWord(word);

        if (dictionaryList.includes(sanitizedWord.toLowerCase())) return previousValue;

        previousValue.push(sanitizedWord);

        return previousValue;
    }, []);

    const suggestedWords = misspelledWords.map((word) => {
        return {
            original: word,
            corrected: didYouMean(word, dictionaryList)
        };
    });

    res.json(suggestedWords);
});

app.get('*', logPath, (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../ui/build', 'index.html'));
});


app.listen(port, async () => {
    await open(`http://localhost:${port}`, {app: {name: 'google chrome'}});
    console.log(`Example app listening at http://localhost:${port}`);
});
