import sanitizeWord from "../services/sanitizeWord";

test('word sanitation works', () => {
    const dirtyWord = "p!otatos\n";
    expect(sanitizeWord(dirtyWord)).toBe('potatos');
});
