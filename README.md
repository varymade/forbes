Hello Forbes. 

So here is what I did. I built an Express server, as well as an React frontend. 
You only have to do the expected command in the root folder as requested via the guide.

I did the heavy lifting where the npm i postinstall script will run install on both the 
server and ui. Then the start, will run a build on the UI followed up by a npm start on the 
server. 

When the server is loaded, it'll launch google chrome with the site. I've tested this process 
many times, however if it fails for any reason (famous "it worked on my machine") then see
the screenshot for the expected results. 

I wanted to have some fun with it, so not only do I have the list of words, but I also added the 
story with red underline for incorrect words. When hovered over, you see the correct word being suggested. 

The project has nodeJS, ExpressJS, React, ESlint, .git and bootstrap. Written in TypeScript.

Thank you for your time. Here you go. 

npm i && npm start. 
