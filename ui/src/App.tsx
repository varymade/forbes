import React, {ReactNodeArray, useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import reactStringReplace from "react-string-replace";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

interface CorrectedSpelling {
    original: string;
    corrected: string;
}

function autoCorrect(story: string, correctedSpellings: CorrectedSpelling[]): any {
   return correctedSpellings.reduce<ReactNodeArray>((modifiedStory, correctedSpelling) => {
       return reactStringReplace(modifiedStory, correctedSpelling.original, (match, index) => {
           return <span key={match} className="correctable">{match}<span className="substitute">{correctedSpelling.corrected}</span></span>;
       })
   }, story as any);
}

function App() {
    const [story, setStory] = useState<string>('Loading story...');
    const [words, setWords] = useState<CorrectedSpelling[]>([]);

    useEffect(() => {
        (async () => {
            const storyResponse = await fetch("/story");
            const storyResponseList = await storyResponse.text();

            if (storyResponseList) {
                setStory(storyResponseList);
            }
        })();
    }, [])

    useEffect(() => {
        (async () => {
            const correctedSpellingResponse = await fetch("/corrected-spelling");
            const correctedSpellingList = await correctedSpellingResponse.json();

            if (correctedSpellingList) {
                setWords(correctedSpellingList);
            }
        })();
    }, []);

    return (
        <Container fluid className="App">
            <Row>
                <h1>Here is your list you requested.</h1>
            </Row>
            <Row>
                <Col>
                    <h2>Misspelled words and their corrected version.</h2>
                    <Row>
                        <Col>Original Word</Col>
                        <Col>Corrected Word</Col>
                    </Row>
                    <hr/>
                    {
                        words.map((wordMap, index) => (
                            <Row key={index}>
                                <Col>
                                    <p>{wordMap.original}</p>
                                </Col>
                                <Col>
                                    <p>{wordMap.corrected}</p>
                                </Col>
                            </Row>

                        ))
                    }
                </Col>
                <Col>
                    <h2>Story</h2>
                    {
                        autoCorrect(story, words)
                    }
                </Col>
            </Row>


        </Container>
    );
}

export default App;
